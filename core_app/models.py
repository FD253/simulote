
from django.core.validators import RegexValidator
from django.db import models
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ValidationError

import datetime as dt
from django.contrib.postgres.fields import IntegerRangeField
from django.core.validators import MinValueValidator, MaxValueValidator
from simulote.users.models import User


TYPE_CHOICES = (
    (1, _('first (1)')),
    (2, _('morning (2)')),
    (3, _('afternoon (3)')),
    (4, _('night (4)'))
)


class Draw(models.Model):
    date = models.DateField()
    type = models.PositiveSmallIntegerField(choices=TYPE_CHOICES)
    n1 = models.CharField(max_length=4, null=True)
    n2 = models.CharField(max_length=4, null=True)
    n3 = models.CharField(max_length=4, null=True)
    n4 = models.CharField(max_length=4, null=True)
    n5 = models.CharField(max_length=4, null=True)
    n6 = models.CharField(max_length=4, null=True)
    n7 = models.CharField(max_length=4, null=True)
    n8 = models.CharField(max_length=4, null=True)
    n9 = models.CharField(max_length=4, null=True)
    n10 = models.CharField(max_length=4, null=True)
    n11 = models.CharField(max_length=4, null=True)
    n12 = models.CharField(max_length=4, null=True)
    n13 = models.CharField(max_length=4, null=True)
    n14 = models.CharField(max_length=4, null=True)
    n15 = models.CharField(max_length=4, null=True)
    n16 = models.CharField(max_length=4, null=True)
    n17 = models.CharField(max_length=4, null=True)
    n18 = models.CharField(max_length=4, null=True)
    n19 = models.CharField(max_length=4, null=True)
    n20 = models.CharField(max_length=4, null=True)

    class Meta:
        unique_together = (('date', 'type'), )


def validate_future(date):
    if date < dt.datetime.today().date():
        raise ValidationError(
            _("This day's draws already finished. Set today or a future day"),
        )
    if dt.datetime.now().today().date() == date:
        if dt.datetime.now().time() >= dt.time(21):
            raise ValidationError(
                _("Today's draws already finished. Set tomorrow or other day"),
            )
    if date.weekday() == 6:
        raise ValidationError(
            _("Date can't be a sunday. "
              "%(value)s is not valid"),
            params={'value': date},
        )


class GenericBet(models.Model):
    owner = models.ForeignKey(User)
    draw = models.ForeignKey(Draw, null=True)
    money = models.PositiveIntegerField()
    date = models.DateField(validators=[validate_future])
    type = models.PositiveSmallIntegerField(choices=TYPE_CHOICES)
    created = models.DateTimeField(auto_now_add=True)
    prize = models.FloatField(null=True)

    class Meta:
        abstract = True

    def clean(self):
        if self.date == dt.datetime.today().date():
            if dt.time(11) <= dt.datetime.now().time() < dt.time(14):
                if self.type <= 1:
                    raise ValidationError(
                        _("You can't bet on this type because has been closed."
                          " Set option 2 or greater or another day")
                    )
            elif dt.time(14) <= dt.datetime.now().time() < dt.time(17):
                if self.type <= 2:
                    raise ValidationError(
                        _("You can't bet on this type because has been closed."
                          " Set option 3 or greater or another day")
                    )
            elif dt.time(17) <= dt.datetime.now().time() < dt.time(21):
                if self.type <= 3:
                    raise ValidationError(
                        _("You can't bet on this type because has been closed."
                          " Set option 4 or greater or another day")
                    )


numeric = RegexValidator(r'^[0-9]*$', 'Only numeric characters are allowed.')


class GenericDirectBet(GenericBet):
    number = models.CharField(max_length=4, validators=[numeric])

    class Meta:
        abstract = True


class ExactBet(GenericDirectBet):
    position = models.PositiveSmallIntegerField(
        default=1, validators=[MinValueValidator(1), MaxValueValidator(20)]
    )


class DirectAndDirectByExtensionBet(GenericDirectBet):
    range = IntegerRangeField(
        help_text='JSON -> example -> '
                  '{"upper":5, "lower":1, "bounds":"[)"}'
                  '"lower" must be greater than 1 and '
                  '"upper" must be lower than 20')


class ParlayBet(GenericBet):
    number1 = models.CharField(max_length=2,
                               validators=[numeric])
    number2 = models.CharField(max_length=2,
                               validators=[numeric])

    range1 = IntegerRangeField(
        help_text='JSON -> example -> '
                  '{"upper":5, "lower":1, "bounds":"[)"} -> '
                  '"lower" must be 1 or greater '
                  'and "upper" must be 20 or lower',
        null=True
    )
    position1 = models.PositiveIntegerField(
        validators=[MinValueValidator(1), MaxValueValidator(20)],
        null=True, help_text='Use this or range1, not both'
    )

    range2 = IntegerRangeField(
        help_text='JSON -> example -> '
                  '{"upper":5, "lower":2, "bounds":"[)"} -> '
                  '"lower" must be 2 or greater '
                  'and "upper" must be 20 or lower',
        null=True
    )
    position2 = models.PositiveIntegerField(
        validators=[MinValueValidator(1), MaxValueValidator(20)],
        null=True, help_text='Use this or range2, not both'
    )

    def clean(self):
        if self.date == dt.datetime.today().date():
            if dt.time(11) <= dt.datetime.now().time() < dt.time(14):
                if self.type <= 1:
                    raise ValidationError(
                        _("You can't bet on this type because is already done."
                          " Set option 2 or greater or another day")
                    )
            elif dt.time(14) <= dt.datetime.now().time() < dt.time(17):
                if self.type <= 2:
                    raise ValidationError(
                        _("You can't bet on this type because is already done."
                          " Set option 3 or greater or another day")
                    )
            elif dt.time(17) <= dt.datetime.now().time() < dt.time(21):
                if self.type <= 3:
                    raise ValidationError(
                        _("You can't bet on this type because is already done."
                          " Set option 4 or greater or another day")
                    )
        if len(self.number1) != 2:
            raise ValidationError(
                _('Number 1 %(value)s must be of length 2'),
                params={'value': self.number1}
            )
        if len(self.number2) != 2:
            raise ValidationError(
                _('Number 2 %(value)s must be of length 2'),
                params={'value': self.number2}
            )
        if self.position1 and self.position2:
            if self.position2 == self.position1:
                raise ValidationError(
                    _('%(value)s is not valid'),
                    params={'value': self.position2},
                )
        if bool(self.range1) == bool(self.position1):
            raise ValidationError(
                _('You need to provide range1 or position1, '
                  'not both, neither none)')
            )
        if bool(self.range2) == bool(self.position2):
            raise ValidationError(
                _('You need to provide range2 or position2,'
                  ' not both, neither none)')
            )
        if self.range1 and self.range2:
            if self.range1.lower < 1 or self.range2.lower < 1:
                raise ValidationError(
                    _('The start of any range can\'t be lower than 1'),
                )
            if self.range1.upper > 20 or self.range2.upper > 20:
                raise ValidationError(
                    _('The end of any range can\'t be greater than 20'),
                )
            if self.range1.lower > self.range1.upper or \
                    self.range2.lower > self.range2.upper:
                raise ValidationError(
                    _('The start of any range can\'t be greater '
                      'than the end of the same range'),
                )
            if self.range1 == self.range2:
                raise ValidationError(
                    _('Range1 can\'t be equal to Range2')
                )
        else:
            if self.range1 and not self.range2:
                if self.range1.lower < 1:
                    raise ValidationError(
                        _('The start of any range can\'t be lower than 1'),
                    )
                if self.range1.upper > 20:
                    raise ValidationError(
                        _('The end of any range can\'t be greater than 20'),
                    )
            if self.range2 and not self.range1:
                if self.range2.lower < 1:
                    raise ValidationError(
                        _('The start of any range can\'t be lower than 1'),
                    )
                if self.range2.upper > 20:
                    raise ValidationError(
                        _('The end of any range can\'t be greater than 20'),
                    )
                if self.range2.lower == 1 and self.position1 == 1:
                    raise ValidationError(
                        _('Range2 must be from 2 when position1 set to 1')
                    )


class Money(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    money = models.FloatField(default=0.0)


@receiver(post_save, sender=User)
def create_user_money(sender, **kwargs):
    Money.objects.get_or_create(user=kwargs.get('instance'))


@receiver(post_save)
def subtract_user_money_on_bet_creation(sender, created,
                                        instance, **kwargs):
    list_of_models = ['ExactBet', 'ParlayBet', 'DirectAndDirectByExtensionBet']
    if sender.__name__ in list_of_models:
        um = Money.objects.get(id=instance.owner.id)
        if created:
            um.money -= instance.money
        else:
            um.money += instance.prize
        um.save()
