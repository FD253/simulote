from django.test import TestCase

# Create your tests here.

from unittest.mock import patch, MagicMock
from .models import Draw, ExactBet, DirectAndDirectByExtensionBet, ParlayBet
from datetime import timedelta, date
from simulote.taskapp.tasks import calculate_exact_direct_prizes, \
    calculate_double_bet_prizes, calculate_direct_and_direct_by_extension_bet_prizes
from simulote.users.models import User
from random import choice
from string import digits


tomorrow = date.today() + timedelta(days=1)
yesterday = date.today() - timedelta(days=1)


class BetUpdateTests(TestCase):

    def setUp(self):
        User.objects.create(name='u1')
        DirectAndDirectByExtensionBet.objects.create(
            range=(1, 5),
            number='0001',
            money=1, date=tomorrow, type='p', owner=User.objects.get()
        )
        ParlayBet.objects.create(
            number1='01', number2='02', money=1, date=tomorrow, type='p', owner=User.objects.get(),
            range1=(1, 5),
            range2=(3, 7)
        )


def get_rand_draw():
    return ''.join(choice(digits) for i in range(4))

SUCCESSES = [0, 7, 70, 600, 3500]


class PrizeCalculationTests(TestCase):

    def setUp(self):
        User.objects.create(name='u1')

    def test_exact_direct_bet_wins_with_4_numbers_and_position(self):
        rn = get_rand_draw()
        type = 'p'
        Draw.objects.create(date=yesterday, type=type, n1=rn)
        bet = ExactBet(
            number=rn, owner=User.objects.get(),
            money=1, date=Draw.objects.get().date, type=Draw.objects.get().type,
        )
        bet.save()
        self.assertFalse(ExactBet.objects.first().draw)

        calculate_exact_direct_prizes(date=yesterday, _type=type)
        self.assertTrue(ExactBet.objects.first().draw)

        self.assertTrue(ExactBet.objects.first().prize)
        self.assertEqual(ExactBet.objects.first().prize, bet.money * SUCCESSES[4])

    def test_exact_direct_bet_wins_with_3_numbers_and_position(self):
        rn = get_rand_draw()
        type = 'f'
        Draw.objects.create(date=yesterday, type=type, n1=rn)
        bet = ExactBet(
            number=rn[-3:], owner=User.objects.get(),
            money=11, date=Draw.objects.get().date, type=Draw.objects.get().type,
        )
        bet.save()

        calculate_exact_direct_prizes(date=yesterday, _type=type)
        self.assertEqual(ExactBet.objects.first().prize, bet.money * SUCCESSES[3])

    def test_exact_direct_bet_wins_with_2_numbers_and_position(self):
        rn = get_rand_draw()
        type = 'p'
        d = Draw(date=yesterday, type=type, n6=rn)
        d.save()
        bet = ExactBet(
            number=rn[-2:], position=6, owner=User.objects.get(), money=423, date=yesterday,
            type=type
        )
        bet.save()

        calculate_exact_direct_prizes(date=yesterday, _type=type)
        self.assertEqual(ExactBet.objects.first().prize, bet.money * SUCCESSES[2])

    def test_exact_direct_bet_wins_with_1_number_and_position(self):
        rn = get_rand_draw()
        type = 'm'
        d = Draw(date=yesterday, type=type, n5=rn)
        d.save()
        bet = ExactBet(
            number=rn[-1:], position=5, owner=User.objects.get(), money=87, date=yesterday,
            type=type
        )
        bet.save()

        calculate_exact_direct_prizes(date=yesterday, _type=type)
        self.assertEqual(ExactBet.objects.first().prize, bet.money * SUCCESSES[1])

    def test_direct_and_direct_by_extension_bet_wins_with_4_numbers_and_2_of_5_successes(self):
        rn1 = '1234'
        rn2 = '2345'
        rn3 = '3456'
        rn4 = '4567'
        type = 'o'
        d = Draw(date=yesterday, type=type, n1=rn1, n2=rn2, n3=rn3, n4=rn4, n5=rn3)
        d.save()
        bet = DirectAndDirectByExtensionBet(
            range=(1, 5), number=rn3, date=yesterday, type=type, money=1, owner=User.objects.get()
        )
        bet.save()

        calculate_direct_and_direct_by_extension_bet_prizes(date=yesterday, _type=type)

        self.assertEqual(DirectAndDirectByExtensionBet.objects.first().prize, bet.money * SUCCESSES[4] * 2 / 5)

    def test_direct_and_direct_by_extension_bet_wins_with_3_numbers_and_3_of_9_successes(self):
        y = '1234'
        n = '4567'
        type = '?'

        d = Draw(date=yesterday, type=type, n1=y, n2=y, n3=y, n4=n, n5=n, n6=n, n7=n, n8=n, n9=n)
        d.save()

        bet = DirectAndDirectByExtensionBet(
            range=(1,9), number=y[-3:], date=yesterday, type=type, money=10, owner=User.objects.get()
        )
        bet.save()

        calculate_direct_and_direct_by_extension_bet_prizes(date=yesterday, _type=type)

        self.assertEqual(DirectAndDirectByExtensionBet.objects.first().prize, bet.money * SUCCESSES[3] / 9 * 3)

    def test_direct_and_direct_by_extension_bet_wins_with_2_numbers_and_1_of_20_successes(self):
        y = '3456'
        n = '8765'
        type = '?'

        d = Draw(date=yesterday, type=type, n1=y, n2=n, n3=n, n4=n, n5=n, n6=n, n7=n, n8=n, n9=n,
                 n10=n,n11=n,n12=n,n13=n,n14=n,n15=n,n16=n,n17=n,n18=n,n19=n,n20=n)
        d.save()

        bet = DirectAndDirectByExtensionBet(
            range=(1, 20), number=y[-2:], date=yesterday, type=type, money=15, owner=User.objects.get()
        )
        bet.save()
        calculate_direct_and_direct_by_extension_bet_prizes(date=yesterday, _type=type)

        self.assertEqual(DirectAndDirectByExtensionBet.objects.first().prize, bet.money * SUCCESSES[2] / 20)

    def test_double_bet_wins_with_2_positions(self):
        n1 = '6533'
        n2 = '8764'
        type = 'f'

        d = Draw(date=yesterday, type=type, n1=n1, n2=n2)
        d.save()

        bet = ParlayBet(
            number1=n1[-2:], position1=1, number2=n2[-2:], position2=2, date=yesterday, type=type, money=1,
            owner=User.objects.get()
        )
        bet.save()

        calculate_double_bet_prizes(date=yesterday, _type=type)
        self.assertEqual(ParlayBet.objects.first().prize, bet.money * SUCCESSES[4])

    def test_double_bet_loses_with_number1_in_position1_and_number2_in_range_from_1_to_20_with_equal_numbers_and_1_success_in_the_range_in_position1(self):
        y = '2452'
        n = '1345'
        type = 'p'

        d = Draw(date=yesterday, type=type, n1=y, n2=n, n3=n, n4=n, n5=n, n6=n, n7=n, n8=n, n9=n,
                 n10=n, n11=n, n12=n, n13=n, n14=n, n15=n, n16=n, n17=n, n18=n, n19=n, n20=n)

        d.save()
        bet = ParlayBet(
            number1=y[-2:], position1=1, number2=y[-2:], range2=(1, 20), money=1, date=yesterday, type=type,
            owner=User.objects.get()
        )
        bet.save()

        calculate_double_bet_prizes(date=yesterday, _type=type)
        self.assertFalse(ParlayBet.objects.get().prize)

    def test_double_bet_wins_with_number1_in_position1_and_number2_in_range_from_1_to_20_with_different_numbers_and_2_success_in_pos_8(self):
        y = '2452'
        y2 = '9876'
        n = '1345'
        type = 'p'

        d = Draw(date=yesterday, type=type, n1=y, n2=n, n3=n, n4=n, n5=n, n6=n, n7=n, n8=y2, n9=n,
                 n10=n, n11=n, n12=n, n13=n, n14=n, n15=n, n16=n, n17=n, n18=n, n19=n, n20=n)

        d.save()
        bet = ParlayBet(
            number1=y[-2:], position1=1, number2=y2[-2:], range2=(1, 20), money=1, date=yesterday, type=type,
            owner=User.objects.get()
        )
        bet.save()

        calculate_double_bet_prizes(date=yesterday, _type=type)
        self.assertAlmostEqual(ParlayBet.objects.get().prize, bet.money * SUCCESSES[2] * SUCCESSES[2] / 19)

    def test_double_bet_wins_with_number1_in_position1_and_number2_in_range_from_2_to_20_with_equal_numbers_and_1_success_in_the_range_in_p2(self):
        y = '2452'
        n = '1345'
        type = 'p'

        d = Draw(date=yesterday, type=type, n1=y, n2=y, n3=n, n4=n, n5=n, n6=n, n7=n, n8=n, n9=n,
                 n10=n, n11=n, n12=n, n13=n, n14=n, n15=n, n16=n, n17=n, n18=n, n19=n, n20=n)

        d.save()
        bet = ParlayBet(
            number1=y[-2:], position1=1, number2=y[-2:], range2=(2, 20), money=1, date=yesterday, type=type,
            owner=User.objects.get()
        )
        bet.save()

        calculate_double_bet_prizes(date=yesterday, _type=type)
        self.assertAlmostEqual(ParlayBet.objects.get().prize, bet.money * SUCCESSES[2] * SUCCESSES[2] / 19)

    def test_double_bet_wins_with_number1_in_position1_and_number2_in_range_from_1_to_20_with_different_numbers_and_1_success_in_the_range_in_p2(self):
        y = '2452'
        y2 = '9876'
        n = '1345'
        type = 'p'

        d = Draw(date=yesterday, type=type, n1=y, n2=y2, n3=n, n4=n, n5=n, n6=n, n7=n, n8=n, n9=n,
                 n10=n, n11=n, n12=n, n13=n, n14=n, n15=n, n16=n, n17=n, n18=n, n19=n, n20=n)

        d.save()
        bet = ParlayBet(
            number1=y[-2:], position1=1, number2=y2[-2:], range2=(1, 20), money=1, date=yesterday, type=type,
            owner=User.objects.get()
        )
        bet.save()

        calculate_double_bet_prizes(date=yesterday, _type=type)
        self.assertAlmostEqual(ParlayBet.objects.get().prize, bet.money * SUCCESSES[2] * SUCCESSES[2] / 19)

    def test_double_bet_wins_with_number1_in_position1_and_number2_in_range_from_5_to_20_with_equal_numbers_and_2_success_in_the_range_in_p20_and_p10(self):
        y = '2452'
        n = '1345'
        type = 'p'

        d = Draw(date=yesterday, type=type, n1=y, n2=n, n3=n, n4=n, n5=n, n6=n, n7=n, n8=n, n9=n,
                 n10=y, n11=n, n12=n, n13=n, n14=n, n15=n, n16=n, n17=n, n18=n, n19=n, n20=y)

        d.save()
        bet = ParlayBet(
            number1=y[-2:], position1=1, number2=y[-2:], range2=(5, 20), money=1, date=yesterday, type=type,
            owner=User.objects.get()
        )
        bet.save()

        calculate_double_bet_prizes(date=yesterday, _type=type)
        self.assertAlmostEqual(ParlayBet.objects.get().prize, bet.money * SUCCESSES[2] * SUCCESSES[2] / 16 * 2)

    def test_double_bet_wins_with_number1_in_position1_and_number2_in_range_from_5_to_20_with_different_numbers_and_2_success_in_the_range_in_p20_and_p10(self):
        y = '2452'
        y2 = '9876'
        n = '1345'
        type = 'p'

        d = Draw(date=yesterday, type=type, n1=y, n2=n, n3=n, n4=n, n5=n, n6=n, n7=n, n8=n, n9=n,
                 n10=y2, n11=n, n12=n, n13=n, n14=n, n15=n, n16=n, n17=n, n18=n, n19=n, n20=y2)

        d.save()
        bet = ParlayBet(
            number1=y[-2:], position1=1, number2=y2[-2:], range2=(5, 20), money=1, date=yesterday, type=type,
            owner=User.objects.get()
        )
        bet.save()

        calculate_double_bet_prizes(date=yesterday, _type=type)
        self.assertAlmostEqual(ParlayBet.objects.get().prize, bet.money * SUCCESSES[2] * SUCCESSES[2] / 16 * 2)

    def test_double_bet_wins_with_number2_in_pos1_and_number1_in_range_from_5_to_20_with_equal_numbers_and_2_success_in_the_range_in_p20_and_p10(self):
        y = '2452'
        n = '1345'
        type = 'p'

        d = Draw(date=yesterday, type=type, n1=y, n2=n, n3=n, n4=n, n5=n, n6=n, n7=n, n8=n, n9=n,
                 n10=y, n11=n, n12=n, n13=n, n14=n, n15=n, n16=n, n17=n, n18=n, n19=n, n20=y)

        d.save()
        bet = ParlayBet(
            number1=y[-2:], range1=(5, 20), number2=y[-2:], position2=1, money=1, date=yesterday, type=type,
            owner=User.objects.get()
        )
        bet.save()

        calculate_double_bet_prizes(date=yesterday, _type=type)
        self.assertAlmostEqual(ParlayBet.objects.get().prize, bet.money * SUCCESSES[2] / 16 * 2 * SUCCESSES[2])

    def test_double_bet_wins_with_number2_in_pos1_and_number1_in_range_from_5_to_20_with_different_numbers_and_2_success_in_the_range_in_p20_and_p10(self):
        y = '2452'
        y2 = '4564'
        n = '1345'
        type = 'p'

        d = Draw(date=yesterday, type=type, n1=y2, n2=n, n3=n, n4=n, n5=n, n6=n, n7=n, n8=n, n9=n,
                 n10=y, n11=n, n12=n, n13=n, n14=n, n15=n, n16=n, n17=n, n18=n, n19=n, n20=y)

        d.save()
        bet = ParlayBet(
            number1=y[-2:], range1=(5, 20), number2=y2[-2:], position2=1, money=1, date=yesterday, type=type,
            owner=User.objects.get()
        )
        bet.save()

        calculate_double_bet_prizes(date=yesterday, _type=type)
        self.assertAlmostEqual(ParlayBet.objects.get().prize, bet.money * SUCCESSES[2] / 16 * 2 * SUCCESSES[2])

    def test_double_bet_loses_with_number2_in_pos5_and_number1_in_range_from_1_to_20_with_equal_numbers_and_2_success_in_the_range_in_p20_and_p10(self):
        y = '2452'
        n = '1345'
        type = 'p'

        d = Draw(date=yesterday, type=type, n1=n, n2=n, n3=n, n4=n, n5=y, n6=n, n7=n, n8=n, n9=n,
                 n10=y, n11=n, n12=n, n13=n, n14=n, n15=n, n16=n, n17=n, n18=n, n19=n, n20=y)

        d.save()
        bet = ParlayBet(
            number1=y[-2:], range1=(1, 20), number2=y[-2:], position2=5, money=1, date=yesterday, type=type,
            owner=User.objects.get()
        )
        bet.save()

        calculate_double_bet_prizes(date=yesterday, _type=type)
        self.assertFalse(ParlayBet.objects.get().prize)

    def test_double_bet_wins_with_number2_in_pos5_and_number1_in_range_from_1_to_20_with_different_numbers_and_2_success_in_the_range_in_p20_and_p10(self):
        y = '2452'
        y2 = '7482'
        n = '1345'
        type = 'p'

        d = Draw(date=yesterday, type=type, n1=n, n2=n, n3=n, n4=n, n5=y2, n6=n, n7=n, n8=n, n9=n,
                 n10=y, n11=n, n12=n, n13=n, n14=n, n15=n, n16=n, n17=n, n18=n, n19=n, n20=y)

        d.save()
        bet = ParlayBet(
            number1=y[-2:], range1=(1, 20), number2=y2[-2:], position2=5, money=1, date=yesterday, type=type,
            owner=User.objects.get()
        )
        bet.save()

        calculate_double_bet_prizes(date=yesterday, _type=type)
        self.assertAlmostEqual(ParlayBet.objects.get().prize, bet.money * SUCCESSES[2] / 19 * 2 * SUCCESSES[2])

    def test_double_bet_wins_with_number2_in_pos5_and_number1_in_range_from_9_to_20_with_equal_numbers_and_2_success_in_the_range_in_p20_and_p10(self):
        y = '2452'
        n = '1345'
        type = 'p'

        d = Draw(date=yesterday, type=type, n1=n, n2=n, n3=n, n4=n, n5=y, n6=n, n7=n, n8=n, n9=n,
                 n10=y, n11=n, n12=n, n13=n, n14=n, n15=n, n16=n, n17=n, n18=n, n19=n, n20=y)

        d.save()
        bet = ParlayBet(
            number1=y[-2:], range1=(9, 20), number2=y[-2:], position2=5, money=1, date=yesterday, type=type,
            owner=User.objects.get()
        )
        bet.save()

        calculate_double_bet_prizes(date=yesterday, _type=type)
        self.assertAlmostEqual(ParlayBet.objects.get().prize, bet.money * SUCCESSES[2] / 12 * 2 * SUCCESSES[2])

    def test_double_bet_wins_with_number2_in_pos5_and_number1_in_range_from_9_to_20_with_different_numbers_and_2_success_in_the_range_in_p20_and_p10(self):
        y = '2452'
        y2 = '7482'
        n = '1345'
        type = 'p'

        d = Draw(date=yesterday, type=type, n1=n, n2=n, n3=n, n4=n, n5=y2, n6=n, n7=n, n8=n, n9=n,
                 n10=y, n11=n, n12=n, n13=n, n14=n, n15=n, n16=n, n17=n, n18=n, n19=n, n20=y)

        d.save()
        bet = ParlayBet(
            number1=y[-2:], range1=(9, 20), number2=y2[-2:], position2=5, money=1, date=yesterday, type=type,
            owner=User.objects.get()
        )
        bet.save()

        calculate_double_bet_prizes(date=yesterday, _type=type)
        self.assertAlmostEqual(ParlayBet.objects.get().prize, bet.money * SUCCESSES[2] / 12 * 2 * SUCCESSES[2])

    def test_double_bet_wins_with_number2_in_range_from_2_to_6_and_number1_in_range_from_9_to_20_with_equal_numbers_and_2_success_in_the_range_in_p20_and_p10(self):
        y = '2452'
        n = '1345'
        type = 'p'

        d = Draw(date=yesterday, type=type, n1=n, n2=n, n3=n, n4=n, n5=y, n6=n, n7=n, n8=n, n9=n,
                 n10=y, n11=n, n12=n, n13=n, n14=n, n15=n, n16=n, n17=n, n18=n, n19=n, n20=y)

        d.save()
        bet = ParlayBet(
            number1=y[-2:], range1=(9, 20), number2=y[-2:], range2=(2, 6), money=1, date=yesterday, type=type,
            owner=User.objects.get()
        )
        bet.save()

        calculate_double_bet_prizes(date=yesterday, _type=type)
        self.assertAlmostEqual(ParlayBet.objects.get().prize, bet.money * SUCCESSES[2] / 12 * 2 * SUCCESSES[2] / 5)

    def test_double_bet_wins_with_number2_in_range_from_2_to_6_and_number1_in_range_from_9_to_20_with_different_numbers_and_2_success_in_the_range_in_p20_and_p10(self):
        y = '2452'
        y2 = '7482'
        n = '1345'
        type = 'p'

        d = Draw(date=yesterday, type=type, n1=n, n2=n, n3=n, n4=n, n5=y2, n6=n, n7=n, n8=n, n9=n,
                 n10=y, n11=n, n12=n, n13=n, n14=n, n15=n, n16=n, n17=n, n18=n, n19=n, n20=y)

        d.save()
        bet = ParlayBet(
            number1=y[-2:], range1=(9, 20), number2=y2[-2:], range2=(2, 6), money=1, date=yesterday, type=type,
            owner=User.objects.get()
        )
        bet.save()

        calculate_double_bet_prizes(date=yesterday, _type=type)
        self.assertAlmostEqual(ParlayBet.objects.get().prize, bet.money * SUCCESSES[2] / 12 * 2 * SUCCESSES[2] / 5)

    def test_double_bet_loses_with_number2_in_range_from_12_to_16_without_success_and_number1_in_range_from_9_to_20_with_equal_numbers_and_2_success_in_the_range_in_p20_and_p10(self):
        y = '2452'
        n = '1345'
        type = 'p'

        d = Draw(date=yesterday, type=type, n1=n, n2=n, n3=n, n4=n, n5=n, n6=n, n7=n, n8=n, n9=n,
                 n10=y, n11=n, n12=n, n13=n, n14=n, n15=n, n16=n, n17=n, n18=n, n19=n, n20=y)

        d.save()
        bet = ParlayBet(
            number1=y[-2:], range1=(9, 20), number2=y[-2:], range2=(12, 16), money=1, date=yesterday, type=type,
            owner=User.objects.get()
        )
        bet.save()

        calculate_double_bet_prizes(date=yesterday, _type=type)
        self.assertFalse(ParlayBet.objects.get().prize)

    def test_double_bet_loses_with_number2_in_range_from_12_to_16_and_number1_in_range_from_9_to_20_with_different_numbers_and_2_success_in_the_range_in_p20_and_p10(self):
        y = '2452'
        y2 = '7482'
        n = '1345'
        type = 'p'

        d = Draw(date=yesterday, type=type, n1=n, n2=n, n3=n, n4=n, n5=y2, n6=n, n7=n, n8=n, n9=n,
                 n10=y, n11=n, n12=n, n13=n, n14=n, n15=n, n16=n, n17=n, n18=n, n19=n, n20=y)

        d.save()
        bet = ParlayBet(
            number1=y[-2:], range1=(9, 20), number2=y2[-2:], range2=(12, 16), money=1, date=yesterday, type=type,
            owner=User.objects.get()
        )
        bet.save()

        calculate_double_bet_prizes(date=yesterday, _type=type)
        self.assertFalse(ParlayBet.objects.get().prize)

    def test_double_bet_wins_with_number2_in_range_from_1_to_16_with_success_in_pos1_and_number1_in_range_from_9_to_20_with_equal_numbers_and_2_success_in_the_range_in_p20_and_p10(self):
        y = '2452'
        n = '1345'
        type = 'p'

        d = Draw(date=yesterday, type=type, n1=y, n2=n, n3=n, n4=n, n5=n, n6=n, n7=n, n8=n, n9=n,
                 n10=y, n11=n, n12=n, n13=n, n14=n, n15=n, n16=n, n17=n, n18=n, n19=n, n20=y)

        d.save()
        bet = ParlayBet(
            number1=y[-2:], range1=(9, 20), number2=y[-2:], range2=(1, 16), money=1, date=yesterday, type=type,
            owner=User.objects.get()
        )
        bet.save()

        calculate_double_bet_prizes(date=yesterday, _type=type)
        self.assertAlmostEqual(ParlayBet.objects.get().prize, bet.money * SUCCESSES[2] / 12 * 2 * SUCCESSES[2] / 16)

    def test_double_bet_wins_with_number2_in_range_from_1_to_16_with_success_in_pos2_and_pos5_and_number1_in_range_from_9_to_20_with_different_numbers_and_2_success_in_the_range_in_p20_and_p10(self):
        y = '2492'
        y2 = '7472'
        n = '1345'
        type = 'p'

        d = Draw(date=yesterday, type=type, n1=n, n2=y2, n3=n, n4=n, n5=y2, n6=n, n7=n, n8=n, n9=n,
                 n10=y, n11=n, n12=n, n13=n, n14=n, n15=n, n16=n, n17=n, n18=n, n19=n, n20=y)

        d.save()
        bet = ParlayBet(
            number1=y[-2:], range1=(9, 20), number2=y2[-2:], range2=(1, 16), money=1, date=yesterday, type=type,
            owner=User.objects.get()
        )
        bet.save()

        calculate_double_bet_prizes(date=yesterday, _type=type)
        self.assertAlmostEqual(ParlayBet.objects.get().prize, bet.money * SUCCESSES[2] / 12 * 2 * SUCCESSES[2] / 16 * 2)

    def test_double_bet_wins_with_number1_in_range_from_1_to_19_with_success_in_pos1_and_number2_in_range_from_9_to_20_with_equal_numbers_and_2_success_in_the_range_in_p20_and_p10(self):
        y = '2452'
        n = '1345'
        type = 'p'

        d = Draw(date=yesterday, type=type, n1=y, n2=n, n3=n, n4=n, n5=n, n6=n, n7=n, n8=n, n9=n,
                 n10=y, n11=n, n12=n, n13=n, n14=n, n15=n, n16=n, n17=n, n18=n, n19=n, n20=y)

        d.save()
        bet = ParlayBet(
            number1=y[-2:], range1=(1, 19), number2=y[-2:], range2=(9, 20), money=1, date=yesterday, type=type,
            owner=User.objects.get()
        )
        bet.save()

        calculate_double_bet_prizes(date=yesterday, _type=type)
        self.assertAlmostEqual(ParlayBet.objects.get().prize, bet.money * SUCCESSES[2] / 19 * 2 * SUCCESSES[2] / 12)

    def test_double_bet_wins_with_number1_in_range_from_1_to_11_with_success_in_pos2_and_pos5_and_number2_in_range_from_9_to_20_with_different_numbers_and_2_success_in_the_range_in_p20_and_p10(self):
        y = '2492'
        y2 = '7472'
        n = '1345'
        type = 'p'

        d = Draw(date=yesterday, type=type, n1=n, n2=y, n3=n, n4=n, n5=y, n6=n, n7=n, n8=n, n9=n,
                 n10=y2, n11=n, n12=n, n13=n, n14=n, n15=n, n16=n, n17=n, n18=n, n19=n, n20=y2)

        d.save()
        bet = ParlayBet(
            number1=y[-2:], range1=(1, 11), number2=y2[-2:], range2=(9, 20), money=1, date=yesterday, type=type,
            owner=User.objects.get()
        )
        bet.save()

        calculate_double_bet_prizes(date=yesterday, _type=type)
        self.assertAlmostEqual(ParlayBet.objects.get().prize, bet.money * SUCCESSES[2] / 11 * 2 * SUCCESSES[2] / 12 * 2)

    def test_double_bet_wins_with_number1_in_range_from_1_to_20_with_success_in_pos1_pos3_and_number2_in_range_from_9_to_20_with_equal_numbers_and_2_success_in_the_range_in_p20_and_p10(self):
        y = '2452'
        n = '1345'
        type = 'p'

        d = Draw(date=yesterday, type=type, n1=y, n2=n, n3=y, n4=n, n5=n, n6=n, n7=n, n8=n, n9=n,
                 n10=y, n11=n, n12=n, n13=n, n14=n, n15=n, n16=n, n17=n, n18=n, n19=n, n20=y)

        d.save()
        bet = ParlayBet(
            number1=y[-2:], range1=(1, 20), number2=y[-2:], range2=(9, 20), money=1, date=yesterday, type=type,
            owner=User.objects.get()
        )
        bet.save()

        calculate_double_bet_prizes(date=yesterday, _type=type)
        self.assertAlmostEqual(ParlayBet.objects.get().prize, bet.money * SUCCESSES[2] / 20 * 3 * SUCCESSES[2] / 12)

    def test_double_bet_wins_with_number1_in_pos1_and_number2_in_range_from_1_to_2_with_equal_numbers_and_success_in_p2_p3_p10_p20(self):
        y = '2452'
        n = '1345'
        type = 'p'

        d = Draw(date=yesterday, type=type, n1=y, n2=y, n3=y, n4=n, n5=n, n6=n, n7=n, n8=n, n9=n,
                 n10=y, n11=n, n12=n, n13=n, n14=n, n15=n, n16=n, n17=n, n18=n, n19=n, n20=y)

        d.save()
        bet = ParlayBet(
            number1=y[-2:], position1=1, number2=y[-2:], range2=(1, 2), money=1, date=yesterday, type=type,
            owner=User.objects.get()
        )
        bet.save()

        calculate_double_bet_prizes(date=yesterday, _type=type)
        self.assertEqual(ParlayBet.objects.get().prize, 2500)

    def test_double_bet_wins_with_number1_in_pos1_and_number2_in_range_from_1_to_3_with_equal_numbers_and_success_in_p2_p3_p10_p20(self):
        y = '2452'
        n = '1345'
        type = 'p'

        d = Draw(date=yesterday, type=type, n1=y, n2=y, n3=y, n4=n, n5=n, n6=n, n7=n, n8=n, n9=n,
                 n10=y, n11=n, n12=n, n13=n, n14=n, n15=n, n16=n, n17=n, n18=n, n19=n, n20=y)

        d.save()
        bet = ParlayBet(
            number1=y[-2:], position1=1, number2=y[-2:], range2=(2, 4), money=1, date=yesterday, type=type,
            owner=User.objects.get()
        )
        bet.save()

        calculate_double_bet_prizes(date=yesterday, _type=type)
        self.assertEqual(ParlayBet.objects.get().prize, 1800)
