from django.contrib import admin
from .models import Draw, ParlayBet, ExactBet, DirectAndDirectByExtensionBet, Money
# Register your models here.

admin.site.register(Draw)
admin.site.register(ParlayBet)
admin.site.register(ExactBet)
admin.site.register(DirectAndDirectByExtensionBet)
admin.site.register(Money)
