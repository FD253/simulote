from core_app.models import Draw, ExactBet, ParlayBet, DirectAndDirectByExtensionBet, Money

from rest_framework import routers, serializers, viewsets, permissions, authentication, filters, fields

from simulote.users.models import User

import django_filters

from rest_framework.views import APIView
from rest_framework.response import Response


class ListUsers(APIView):
    """
    View to list all users in the system.

    * Requires token authentication.
    * Only admin users are able to access this view.
    """
    authentication_classes = (authentication.BasicAuthentication,
                              authentication.TokenAuthentication)
    permission_classes = (permissions.IsAdminUser,)

    def get(self, request, format=None):
        """
        Return a list of all users.
        """
        users = [(user.username, user.money.money)
                 for user in User.objects.all() if user.is_active]
        return Response(users)


class DefaultsMixin(object):
    authentication_classes = (
        authentication.BasicAuthentication,
        authentication.TokenAuthentication,
    )
    permission_classes = (
        permissions.IsAuthenticated,
    )
    paginate_by = 5
    paginate_by_param = 'page_size'
    max_paginate_by = 100
    filter_backends = (
        filters.DjangoFilterBackend,
        filters.SearchFilter,
        filters.OrderingFilter,
    )


class UserSerializer(serializers.HyperlinkedModelSerializer):
    money = serializers.FloatField(source='money.money')

    class Meta:
        model = User
        fields = ('id', 'url', User.USERNAME_FIELD, 'money')


class UserViewSet(DefaultsMixin, viewsets.ReadOnlyModelViewSet):
    queryset = User.objects.order_by(User.USERNAME_FIELD)
    serializer_class = UserSerializer

    def get_queryset(self):
        return User.objects.filter(username=self.request.user)


class DrawSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Draw


class DrawViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Draw.objects.all()
    serializer_class = DrawSerializer
    permission_classes = [permissions.AllowAny]


class ExactBetSerializer(serializers.HyperlinkedModelSerializer):
    owner = serializers.PrimaryKeyRelatedField(
        read_only=True, default=fields.CurrentUserDefault()
    )

    class Meta:
        model = ExactBet
        fields = ('id', 'url', 'owner', 'money', 'prize',
                  'number', 'position', 'date', 'type', 'draw')

    def validate(self, attrs):
        instance = ExactBet(**attrs)
        instance.clean()
        return attrs


class GenericBetFilter(django_filters.FilterSet):
    date_min = django_filters.DateFilter(name='date', lookup_expr='gte')
    date_max = django_filters.DateFilter(name='date', lookup_expr='lte')
    draw = django_filters.BooleanFilter(lookup_expr='isnull')

    class Meta:
        fields = ('date_min', 'date_max', 'draw')


class ExactBetFilter(GenericBetFilter):
    class Meta:
        model = ExactBet


class ExactBetViewSet(DefaultsMixin, viewsets.ModelViewSet):
    queryset = ExactBet.objects.all().order_by('date')
    serializer_class = ExactBetSerializer
    filter_class = ExactBetFilter
    permission_classes = [permissions.IsAuthenticated]
    ordering_fields = ('date', 'type', 'money')

    def get_queryset(self):
        user = self.request.user
        return ExactBet.objects.filter(owner=user).order_by('date')

    def perform_update(self, serializer):
        pass

    def perform_destroy(self, instance):
        pass

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class ParlayBetSerializer(serializers.HyperlinkedModelSerializer):
    owner = serializers.PrimaryKeyRelatedField(
       read_only=True, default=fields.CurrentUserDefault()
    )

    class Meta:
        model = ParlayBet
        fields = ('id', 'owner', 'url', 'money', 'prize', 'number1',
                  'position1', 'range1', 'number2', 'position2',
                  'range2', 'date', 'type', 'draw')

    def validate(self, attrs):
        instance = ParlayBet(**attrs)
        instance.clean()
        return attrs


class ParlayBetFilter(GenericBetFilter):
    class Meta:
        model = ParlayBet


class ParlayBetViewSet(DefaultsMixin, viewsets.ModelViewSet):
    queryset = ParlayBet.objects.order_by('date')
    serializer_class = ParlayBetSerializer
    filter_class = ParlayBetFilter
    permission_classes = [permissions.IsAuthenticated]
    ordering_fields = ('date', 'type', 'money')

    def get_queryset(self):
        user = self.request.user
        return ParlayBet.objects.filter(owner=user).order_by('date')

    def perform_update(self, serializer):
        pass

    def perform_destroy(self, instance):
        pass

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class DirectAndDirectByExtensionBetSerializer(
    serializers.HyperlinkedModelSerializer):
    owner = serializers.PrimaryKeyRelatedField(
        read_only=True, default=fields.CurrentUserDefault()
    )

    class Meta:
        model = DirectAndDirectByExtensionBet
        fields = ('id', 'url', 'owner', 'money', 'prize', 'number',
                  'range', 'date', 'type', 'draw')

    def validate(self, attrs):
        instance = DirectAndDirectByExtensionBet(**attrs)
        instance.clean()
        return attrs


class DirectBetFilter(GenericBetFilter):
    class Meta:
        model = DirectAndDirectByExtensionBet


class DirectAndDirectByExtensionBetViewSet(DefaultsMixin,
                                           viewsets.ModelViewSet):
    queryset = DirectAndDirectByExtensionBet.objects.all().order_by('date')
    serializer_class = DirectAndDirectByExtensionBetSerializer
    filter_class = DirectBetFilter
    ordering_fields = ('date', 'type', 'money')

    def get_queryset(self):
        user = self.request.user
        return DirectAndDirectByExtensionBet.objects.filter(owner=user).\
            order_by('date')

    def perform_update(self, serializer):
        pass

    def perform_destroy(self, instance):
        pass

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter(trailing_slash=False,
                               schema_title='Simulote API')
router.register(r'users', UserViewSet)
router.register(r'draws', DrawViewSet)
router.register(r'exacts', ExactBetViewSet)
router.register(r'directs', DirectAndDirectByExtensionBetViewSet)
router.register(r'parlays', ParlayBetViewSet)

