from __future__ import absolute_import

from .celery import app
import datetime
import requests
from bs4 import BeautifulSoup
import django
from core_app.models import Draw, ExactBet, DirectAndDirectByExtensionBet, ParlayBet
django.setup()


@app.task
def calculate_exact_direct_prizes(date, _type):

    attrs = ['offset']
    for i in range(20):
        attrs.append('n'+str(i+1))

    success_4 = 3500
    success_3 = 600
    success_2 = 70
    success_1 = 7

    exact_direct_bets = ExactBet.objects.filter(
        date=date, type=_type, draw=None
    )

    draw = Draw.objects.get(date=date, type=_type)
    for bet in exact_direct_bets:
        bet.draw = draw
        if bet.number == getattr(draw, attrs[bet.position]):
            bet.prize = bet.money * success_4
        elif bet.number == getattr(draw, attrs[bet.position])[-3:]:
            bet.prize = bet.money * success_3
        elif bet.number == getattr(draw, attrs[bet.position])[-2:]:
            bet.prize = bet.money * success_2
        elif bet.number == getattr(draw, attrs[bet.position])[-1:]:
            bet.prize = bet.money * success_1
        bet.save()


@app.task
def calculate_direct_and_direct_by_extension_bet_prizes(date, _type):

    attrs = ['offset']
    for i in range(20):
        attrs.append('n'+str(i+1))

    success_4 = 3500
    success_3 = 600
    success_2 = 70
    success_1 = 7

    direct_and_direct_by_extension_bets = DirectAndDirectByExtensionBet.objects.filter(
        date=date, type=_type, draw=None
    )

    draw = Draw.objects.get(date=date, type=_type)
    for bet in direct_and_direct_by_extension_bets:
        bet.draw = draw
        bet_range = bet.range.upper - bet.range.lower + 1
        bet.prize = 0
        bet_money = bet.money
        for i in range(bet.range.lower, bet.range.upper + 1):
            if getattr(draw, 'n'+str(i)) == bet.number:
                bet.prize += bet_money * success_4 / bet_range
            elif getattr(draw, 'n'+str(i))[-3:] == bet.number:
                bet.prize += bet_money * success_3 / bet_range
            elif getattr(draw, 'n'+str(i))[-2:] == bet.number:
                bet.prize += bet_money * success_2 / bet_range
            elif getattr(draw, 'n'+str(i))[-1:] == bet.number:
                bet.prize += bet_money * success_1 / bet_range

        if bet.prize == 0:
            bet.prize = None
        bet.save()


@app.task
def calculate_double_bet_prizes(date, _type):

    attrs = ['offset']
    for i in range(20):
        attrs.append('n'+str(i+1))

    simple_success = 70
    exact_success = 3500

    by_extension_double_bets = ParlayBet.objects.filter(
        date=date, type=_type, draw=None
    )

    draw = Draw.objects.get(date=date, type=_type)
    for bet in by_extension_double_bets:
        bet.draw = draw
        money = bet.money
        try:
            length_range1 = bet.range1.upper - bet.range1.lower + 1
        except AttributeError:
            length_range1 = 1
        try:
            length_range2 = bet.range2.upper - bet.range2.lower + 1
        except AttributeError:
            length_range2 = 1

        if length_range1 == 1 and length_range2 == 1:
            if (bet.number1 == getattr(draw, attrs[bet.position1])[-2:] and
                    bet.number2 == getattr(draw, attrs[bet.position2])[-2:]):
                bet.prize = money * exact_success

        elif length_range2 == 1 and length_range1 > 1:
            success_range1 = list()
            for i in range(1, 21):
                if bet.range1.lower <= i <= bet.range1.upper:
                    if bet.number1 == getattr(draw, 'n'+str(i))[-2:]:
                        success_range1.append(i)
            if bet.number2 == getattr(draw, attrs[bet.position2])[-2:]:
                if bet.number1 != bet.number2:
                    if bet.range1.lower <= bet.position2 <= bet.range1.upper:
                        bet.prize = money * simple_success / (length_range1 - 1) * len(success_range1)
                    else:
                        bet.prize = money * simple_success / length_range1 * len(success_range1)
                else:
                    if bet.range1.lower <= bet.position2 <= bet.range1.upper:
                        bet.prize = 0
                    else:
                        bet.prize = money * simple_success / length_range1 * len(success_range1)
                bet.prize *= simple_success

        elif length_range1 == 1 and length_range2 > 1:
            success_range2 = list()
            for i in range(1, 21):
                if bet.range2.lower <= i <= bet.range2.upper:
                    if bet.number2 == getattr(draw, 'n'+str(i))[-2:]:
                        success_range2.append(i)
            if bet.number1 == getattr(draw, attrs[bet.position1])[-2:]:
                bet.prize = bet.money * simple_success
                if bet.number1 != bet.number2:
                    if bet.range2.lower <= bet.position1 <= bet.range2.upper:
                        bet.prize = bet.prize * simple_success / (length_range2 - 1) * len(success_range2)
                    else:
                        bet.prize = bet.prize * simple_success / length_range2 * len(success_range2)
                else:
                    if bet.range2.lower <= bet.position1 <= bet.range2.upper:
                        success_range2.remove(bet.position1)
                        bet.prize = bet.prize * simple_success / (length_range2 - 1) * len(success_range2)
                    else:
                        bet.prize = bet.prize * simple_success / length_range2 * len(success_range2)

        elif length_range1 > 1 and length_range2 > 1:
            success_range1 = list()
            success_range2 = list()
            for i in range(1, 21):
                if bet.range1.lower <= i <= bet.range1.upper:
                    if bet.number1 == getattr(draw, 'n'+str(i))[-2:]:
                        success_range1.append(i)
                if bet.range2.lower <= i <= bet.range2.upper:
                    if bet.number2 == getattr(draw, 'n'+str(i))[-2:]:
                        success_range2.append(i)
            if bet.number1 != bet.number2:
                bet.prize = bet.money * simple_success / length_range1 * len(success_range1)
                bet.prize *= simple_success / length_range2 * len(success_range2)
            else:
                for i in success_range1:
                    if i in success_range2:
                        success_range2.remove(i)
                        break
                for i in success_range2:
                    if i in success_range1:
                        success_range1.remove(i)
                bet.prize = bet.money * simple_success / length_range1 * len(success_range1)
                bet.prize *= simple_success / length_range2 * len(success_range2)
        if length_range1 == 1 and length_range2 == 1:
            pass
        elif bet.prize > bet.money * 2500 and length_range1 == 1 and length_range2 == 2:
            bet.prize = bet.money * 2500
        elif bet.prize > bet.money * 1800 and length_range1 == 1 and length_range2 == 3:
            bet.prize = bet.money * 1800
        elif bet.prize > bet.money * 1500:
            bet.prize = bet.money * 1500
        bet.save()


@app.task
def new_draw(draw_type):

    base = "http://www.nacionalloteria.com/argentina/quiniela-nacional.php"
    base_date = "del-dia"
    today = datetime.date.today()
    base_type = "periodo"
    _type = draw_type
    if _type == 1:
        _type = 'primera'
    elif _type == 2:
        _type = 'matutina'
    elif _type == 3:
        _type = 'vespertina'
    elif _type == 4:
        _type = 'nocturna'
    url = base + '?' + base_date + '=' + str(today) + '&' + base_type + '=' + _type
    print(url)
    r = requests.get(url)

    soup = BeautifulSoup(r.text, 'html.parser')

    raw = soup.find_all('td', class_='celNor')
    d = Draw.objects.create(
        n1=raw[1].text,
        n2=raw[3].text,
        n3=raw[5].text,
        n4=raw[7].text,
        n5=raw[9].text,
        n6=raw[11].text,
        n7=raw[13].text,
        n8=raw[15].text,
        n9=raw[17].text,
        n10=raw[19].text,
        n11=raw[21].text,
        n12=raw[23].text,
        n13=raw[25].text,
        n14=raw[27].text,
        n15=raw[29].text,
        n16=raw[31].text,
        n17=raw[33].text,
        n18=raw[35].text,
        n19=raw[37].text,
        n20=raw[39].text,
        date=datetime.date.today(),
        type=draw_type[0]
    )
    d.save()
