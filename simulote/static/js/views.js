(function ($, Backbone, _, app) {

    var TemplateView = Backbone.View.extend({
        templateName: '',
        initialize: function () {
            this.template = _.template($(this.templateName).html());
        },
        render: function () {
            var context = this.getContext(),
                html = this.template(context);
            this.$el.html(html);
        },
        getContext: function () {
            return {};
        }
    });

    var FormView = TemplateView.extend({
        events: {
            'submit form' : 'submit'
        },
        errorTemplate: _.template('<span class="error"><%- msg %></span>'),
        clearErrors: function () {
            $('.error', this.form).remove();
        },
        showErrors: function (errors) {
            _.map(errors, function(fieldErrors, name){
                var field = $(':input[name=' + name + ']', this.form),
                    label = $('label[for=' + field.attr('id')+ ']', this.form);
                if (label.length === 0) {
                    label = $('label', this.form).first();
                }
                function appendError(msg) {
                    label.before(this.errorTemplate({msg: msg}));
                }
                _.map(fieldErrors, appendError, this);
            }, this);
        },
        serializeForm: function(form){
            return _.object(_.map(form.serializeArray(), function(item){
                // Convert object to tuple of (name, value)
                return [item.name, item.value];
            }));
        },
        submit: function (event) {
            event.preventDefault();
            this.form = $(event.currentTarget);
            this.clearErrors;
        },
        failure: function (xhr, status, error) {
            var errors = xhr.responseJSON;
            this.showErrors(errors);
        },
        done: function (event) {
            if (event){
                event.preventDefault();
            }
            this.trigger('done');
            this.remove();
        },
        modelFailure: function (model, xhr, options) {
            var errors = xhr.responseJSON;
            this.showErrors(errors)
        },

    });

    var NewParlayView = FormView.extend({
        templateName: '#new-parlay-template',
        className: 'new-parlay',
        events: _.extend({
            'click button.cancel': 'done',
        }, FormView.prototype.events),
        submit: function (event) {
            var self = this,
                attributes = {};
            FormView.prototype.submit.apply(this, arguments);
            attributes = this.serializeForm(this.form);
            app.collections.ready.done(function () {
                app.parlays.create(attributes, {
                    wait: true,
                    success: $.proxy(self.success, self),
                    error: $.proxy(self.modelFailure, self),
                });
            })
        },
        success: function (model) {
            this.done();
            window.location.hash = '#parlay/' + model.get('id');
        }
    });
    var NewDirectView = FormView.extend({
        templateName: '#new-direct-template',
        className: 'new-direct',
        events: _.extend({
            'click button.cancel': 'done',
        }, FormView.prototype.events),
        submit: function (event) {
            var self = this,
                attributes = {};
            FormView.prototype.submit.apply(this, arguments);
            attributes = this.serializeForm(this.form);
            app.collections.ready.done(function () {
                app.directs.create(attributes, {
                    wait: true,
                    success: $.proxy(self.success, self),
                    error: $.proxy(self.modelFailure, self),
                });
            })
        },
        success: function (model) {
            this.done();
            window.location.hash = '#direct/' + model.get('id');
        }
    });
    var NewExactView = FormView.extend({
        templateName: '#new-exact-template',
        className: 'new-exact',
        events: _.extend({
            'click button.cancel': 'done',
        }, FormView.prototype.events),
        submit: function (event) {
            var self = this,
                attributes = {};
            FormView.prototype.submit.apply(this, arguments);
            attributes = this.serializeForm(this.form);
            app.collections.ready.done(function () {
                app.exacts.create(attributes, {
                    wait: true,
                    success: $.proxy(self.success, self),
                    error: $.proxy(self.modelFailure, self),
                });
            })
        },
        success: function (model) {
            this.done();
            window.location.hash = '#exact/' + model.get('id');
        }
    });

    var AllBetsView = TemplateView.extend({
        events: {
            'click button.addParlay': 'renderAddParlayForm',
            'click button.addExact': 'renderAddExactForm',
            'click button.addDirect': 'renderAddDirectForm'
        },
        getContext: function () {
            return {parlays: app.parlays || null,
                exacts: app.exacts || null,
                directs: app.directs || null,
            };
        },
        renderAddDirectForm: function (event) {
            var view = new NewDirectView(),
                link = $(event.currentTarget);
            event.preventDefault;
            link.before(view.el);
            link.hide();
            view.render();
            view.on('done', function () {
                link.show();
            });
        },
        renderAddParlayForm: function (event) {
            var view = new NewParlayView(),
                link = $(event.currentTarget);
            event.preventDefault;
            link.before(view.el);
            link.hide();
            view.render();
            view.on('done', function () {
                link.show();
            });
        },
        renderAddExactForm: function (event) {
            var view = new NewExactView(),
                link = $(event.currentTarget);
            event.preventDefault;
            link.before(view.el);
            link.hide();
            view.render();
            view.on('done', function () {
                link.show();
            });
        },
    });

    var TodayBetsView = AllBetsView.extend({
        templateName: '#today-template',
        initialize: function (options) {
            var self = this;
            TemplateView.prototype.initialize.apply(this, arguments);
            app.collections.ready.done(function () {
                var date = new Date();
                date.setDate(date.getDate());
                date.setHours(date.getHours()-3)
                date = date.toISOString().replace(/T.*/g, '');
                app.exacts.fetch({
                    data: {date_min:date, date_max:date, limit:'99'},
                    success: $.proxy(self.render, self)
                });
                app.parlays.fetch({
                    data: {date_min:date, date_max:date, limit:'99'},
                    success: $.proxy(self.render, self)
                });
                app.directs.fetch({
                    data: {date_min:date, date_max:date, limit:'99'},
                    success: $.proxy(self.render, self)
                })
            });
        },
    });
    var PreviousBetsView = AllBetsView.extend({
        templateName: '#previous-template',
        initialize: function (options) {
            var self = this;
            TemplateView.prototype.initialize.apply(this, arguments);
            app.collections.ready.done(function () {
                var date = new Date();
                date.setDate(date.getDate());
                date.setHours(date.getHours()-3)
                date = date.toISOString().replace(/T.*/g, '');
                app.exacts.fetch({
                    data: {date_max:date, draw:'False', ordering:'-date', limit:'6'},
                    success: $.proxy(self.render, self)
                });
                app.parlays.fetch({
                    data: {date_max:date, draw:'False', ordering:'-date', limit:'6'},
                    success: $.proxy(self.render, self)
                });
                app.directs.fetch({
                    data: {date_max:date, draw:'False', ordering:'-date', limit:'6'},
                    success: $.proxy(self.render, self)
                })
            });
        },
    });
    var NextBetsView = AllBetsView.extend({
        templateName: '#next-template',
        initialize: function (options) {
            var self = this;
            TemplateView.prototype.initialize.apply(this, arguments);
            app.collections.ready.done(function () {
                var date = new Date();
                date.setDate(date.getDate()+1);
                date.setHours(date.getHours()-3)
                date = date.toISOString().replace(/T.*/g, '');
                app.exacts.fetch({
                    data: {date_min:date, draw:'True', limit:'6'},
                    success: $.proxy(self.render, self)
                });
                app.parlays.fetch({
                    data: {date_min:date, draw:'True', limit:'6'},
                    success: $.proxy(self.render, self)
                });
                app.directs.fetch({
                    data: {date_min:date, draw:'True', limit:'6'},
                    success: $.proxy(self.render, self)
                })
            });
        },
    });

    var LoginView = FormView.extend({
        id: 'login',
        templateName: '#login-template',
        submit: function(event){
            var data = {};
            FormView.prototype.submit.apply(this, arguments);
            data = this.serializeForm(this.form);
            $.post(app.apiLogin, data)
                .done($.proxy(this.loginSuccess, this))
                .fail($.proxy(this.failure, this));
        },
        loginSuccess: function(data){
            app.session.save(data.token);
            this.done();
        }
    });

    var HeaderView = TemplateView.extend({
        tagName: 'header',
        templateName: '#header-template',
        events: {
            'click a.logout': 'logout'
        },
        getContext: function () {
            return {authenticated: app.session.authenticated()};
        },
        logout: function (event) {
            event.preventDefault();
            app.session.delete();
            window.location = '/';
        }
    });

    var HomeView = TemplateView.extend({
        templateName: '#home-template',
        initialize: function (options) {
            var self = this;
            TemplateView.prototype.initialize.apply(this, arguments);
            app.collections.ready.done(function () {
                app.users.fetch({
                    data: {limit:'1'},
                    success: $.proxy(self.render, self)
                });
            });
        },
        getContext: function () {
            return {users: app.users || null}
        }
    });

    var DirectView = TemplateView.extend({
        templateName: '#direct-template',
        initialize: function (options) {
            var self = this;
            TemplateView.prototype.initialize.apply(this, arguments);
            this.directId = options.directId;
            this.direct = null;
            app.collections.ready.done(function () {
                app.directs.getOrFetch(self.directId).done(function (direct) {
                    self.direct = direct;
                    self.render();
                }).fail(function (direct) {
                    self.direct = direct;
                    self.direct.invalid = true;
                    self.render();
                });
            });
        },
        getContext: function () {
            return {direct: this.direct};
        }
    });
    var ExactView = TemplateView.extend({
        templateName: '#exact-template',
        initialize: function (options) {
            var self = this;
            TemplateView.prototype.initialize.apply(this, arguments);
            this.exactId = options.exactId;
            this.exact = null;
            app.collections.ready.done(function () {
                app.exacts.getOrFetch(self.exactId).done(function (exact) {
                    self.exact = exact;
                    self.render();
                }).fail(function (exact) {
                    self.exact = exact;
                    self.exact.invalid = true;
                    self.render();
                });
            });
        },
        getContext: function () {
            return {exact: this.exact};
        }
    });
    var ParlayView = TemplateView.extend({
        templateName: '#parlay-template',
        initialize: function (options) {
            var self = this;
            TemplateView.prototype.initialize.apply(this, arguments);
            this.parlayId = options.parlayId;
            this.parlay = null;
            app.collections.ready.done(function () {
                app.parlays.getOrFetch(self.parlayId).done(function (parlay) {
                    self.parlay = parlay;
                    self.render()
                }).fail(function (parlay) {
                    self.parlay = parlay;
                    self.parlay.invalid = true;
                    self.render();
                });
            });
        },
        getContext: function () {
            return {parlay: this.parlay};
        }
    });

    app.views.NextBetsView = NextBetsView;
    app.views.PreviousBetsView = PreviousBetsView;
    app.views.TodayBetsView = TodayBetsView;
    app.views.LoginView = LoginView;
    app.views.HeaderView = HeaderView;
    app.views.ParlayView = ParlayView;
    app.views.ExactView = ExactView;
    app.views.DirectView = DirectView;
    app.views.HomeView = HomeView;

})(jQuery, Backbone, _, app);
