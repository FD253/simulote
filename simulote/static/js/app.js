var app = (function ($) {
    var config = $("#config"),
        app = JSON.parse(config.text());

    $(document).ready(function () {
        //initialize after DOM ready
        var router = new app.router();
    });

    return app;
})(jQuery);
