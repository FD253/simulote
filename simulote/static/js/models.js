(function ($, Backbone, _, app) {

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie !== '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) === (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

    // Setup jQuery ajax calls to handle CSRF
    $.ajaxPrefilter(function (settings, originalOptions, xhr) {
        var csrftoken;
        if (!csrfSafeMethod(settings.type) && !this.crossDomain){
            // Send the token to same-origin, relative URLs only.
            // Send the token only if the method warrants CSRF protection
            // Using the CSRFToken value acquired earlier
            csrftoken = getCookie('csrftoken');
            xhr.setRequestHeader('X-CSRFToken', csrftoken);
        }

    });

    var Session = Backbone.Model.extend({
        defaults: {
            token: null
        },
        initialize: function (options) {
            this.options = options;
            $.ajaxPrefilter($.proxy(this._setupAuth, this));
            this.load();
        },
        load: function () {
            var token = localStorage.apiToken;
            if (token) {
                this.set('token', token);
            }
        },
        save: function (token) {
            this.set('token', token);
            if (token === null) {
                localStorage.removeItem('apiToken');
            } else {
                localStorage.apiToken = token;
            }
        },
        delete: function () {
            this.save(null);
        },
        authenticated: function () {
            return this.get('token') !== null;
        },
        _setupAuth: function (settings, originalOptions, xhr) {
            if (this.authenticated()){
                xhr.setRequestHeader(
                    'Authorization',
                    'Token ' + this.get('token')
                );
            }
        }
    });

    app.session = new Session();

    var BaseModel = Backbone.Model.extend({
        //Overrides the default URL construction
        //Starts by looking for the self value from the links att
        url: function () {
            var links = this.get('links'),
                url = links && links.self;
            //If API doesn't gives URL -> constructed using original bb method
            if (!url) {
                url = Backbone.Model.prototype.url.call(this);
            }
            return url;
        }
    });

    // Stubs for each model
    app.models.Draw = BaseModel.extend({});
    app.models.Parlay = BaseModel.extend({});
    app.models.Exact = BaseModel.extend({});
    app.models.Direct = BaseModel.extend({});
    app.models.User = BaseModel.extend({});

    var BaseCollection = Backbone.Collection.extend({
        parse: function (response) {
            this._next = response.next;
            this._previous = response.previous;
            this._count = response.count;
            return response.results || [];
        },
        getOrFetch: function (id) {
            var result = new $.Deferred(),
                model = this.get(id);
            if (!model) {
                model = this.push ({id: id});
                model.fetch({
                    success: function (model, response, options) {
                        result.resolve(model);
                    },
                    error: function (mode, response, options) {
                        result.reject(model, response);
                    }
                });
            } else {
                result.resolve(model);
            }
            return result;
        }
    });

    //Fetch api root.
    //AJAX deferred object stored
    //This allows other parts of the app to wait until collections are ready
    app.collections.ready = $.getJSON(app.apiRoot);
    app.collections.ready.done(function (data) {

        // When response comes back the resulting URLs are used to build each col
        app.collections.Draws = BaseCollection.extend({
            model: app.models.Draw,
            url: data.draws
        });
        app.draws = new app.collections.Draws(); //Instance

        app.collections.Parlays = BaseCollection.extend({
            model: app.models.Parlay,
            url: data.parlays
        });
        app.parlays = new app.collections.Parlays();

        app.collections.Directs = BaseCollection.extend({
            model: app.models.Direct,
            url: data.directs
        });
        app.directs = new app.collections.Directs();

        app.collections.Exacts = BaseCollection.extend({
            model: app.models.Exact,
            url: data.exacts,
        });
        app.exacts = new app.collections.Exacts();

        app.collections.Users = BaseCollection.extend({
            model: app.models.User,
            url: data.users
        });
        app.users = new app.collections.Users();
    });

})(jQuery, Backbone, _, app);
