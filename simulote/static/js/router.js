(function ($, Backbone, _, app) {
    var AppRouter = Backbone.Router.extend({
        routes: {
            'today': 'today',
            'previous': 'previous',
            'next': 'next',
            'parlay/:id': 'parlay',
            'exact/:id': 'exact',
            'direct/:id': 'direct',
            '': 'home',
        },
        initialize: function (options) {
            this.contentElement = '#content';
            this.current = null;
            this.header = new app.views.HeaderView();
            $('body').prepend(this.header.el);
            this.header.render();
            Backbone.history.start();
        },
        home: function () {
            var view = new app.views.HomeView({el:this.contentElement})
            this.render(view)
        },
        today: function() {
            var view = new app.views.TodayBetsView({el: this.contentElement});
            this.render(view);
        },
        next: function() {
            var view = new app.views.NextBetsView({el: this.contentElement});
            this.render(view);
        },
        previous: function(){
            var view = new app.views.PreviousBetsView({el: this.contentElement});
            this.render(view);
        },
        parlay: function (id) {
            var view = new app.views.ParlayView({
                el: this.contentElement,
                parlayId: id
            });
            this.render(view);
        },
        direct: function (id) {
            var view = new app.views.DirectView({
                el: this.contentElement,
                directId: id
            });
        },
        exact: function (id) {
            var view = new app.views.ExactView({
                el: this.contentElement,
                exactId: id
            });
        },
        route: function (route, name, callback) {
            // Override default route to enforce login on every page
            var login;
            callback = callback || this[name];
            callback = _.wrap(callback, function (original) {
                var args = _.without(arguments, original);
                if (app.session.authenticated()) {
                    original.apply(this, args);
                } else {
                    $(this.contentElement).hide();
                    login = new app.views.LoginView();
                    $(this.contentElement).after(login.el);
                    login.on('done', function(){
                        this.header.render();
                        $(this.contentElement).show();
                        original.apply(this, args);
                    }, this);
                    login.render();
                }
            });
            return Backbone.Router.prototype.route.apply(this, [route, name, callback]);
        },
        render: function (view) {
            if (this.current) {
                this.current.undelegateEvents();
                this.current.$el = $();
                this.current.remove();
            }
            this.current = view;
            this.current.render();
        }
    });

    app.router = AppRouter;

})(jQuery, Backbone, _, app);
